import * as api from '../api';
import { dict, compute } from '@fast-crud/fast-crud';
import { useMessage } from 'naive-ui';
export default function ({ expose }) {
  const pageRequest = async (query) => {
    return await api.GetList(query);
  };
  const message = useMessage();
  return {
    crudOptions: {
      request: {
        pageRequest,
      },
      search: {
        show: true,
      },
      actionbar: {
        show: false,
      },
      toolbar: {
        show: false,
      },
      rowHandle: {
        show: false,
      },
      columns: {
        _checked: {
          title: '选择',
          form: { show: false },
          column: {
            type: 'selection',
            align: 'center',
            width: '55px',
            columnSetDisabled: true, //禁止在列设置中选择
            selectable(row, index) {
              return row.id !== 1; //设置第一行不允许选择
            },
          },
        },
        id: {
          title: 'ID',
          key: 'id',
          type: 'text',
          column: {
            show: false
          },
        },
        name: {
          title: '姓名',
          key: 'name',
          type: 'text',
          search: { show: true },
          column: {
            width: 70,
          },
          form: {
            show: true,
          },
        },
        code: {
          title: '员工号',
          key: 'code',
          type: 'text',
          column: {
            width: 80,
          },
        },
      },
    },
  };
}
